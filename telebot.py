#!/usr/bin/python3

import json
import requests
import time
from collections import OrderedDict


class InvalidToken(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__("Invalid token passed!")


class ReplyKeyboard:

    def __init__(self, markup=[], remove_keyboard=False, resize_keyboard=False, one_time_keyboard=False, selective=False, inline_keyboard=[]):
        self.markup = markup
        self.remove_keyboard = remove_keyboard
        self.resize_keyboard = resize_keyboard
        self.one_time_keyboard = one_time_keyboard 
        self.selective = selective
        self.inline_keyboard = inline_keyboard

    def make_keyboard(self):
        if self.remove_keyboard:
            keyboard = json.dumps({'remove_keyboard': self.remove_keyboard, 'selective': self.selective})
        elif self.inline_keyboard:
            keyboard = json.dumps({'inline_keyboard': self.inline_keyboard})
        else:
            keyboard = json.dumps({'keyboard' : self.markup, 'one_time_keyboard': self.one_time_keyboard, 'resize_keyboard': self.resize_keyboard, 'selective': self.selective})
        return keyboard


class Bot:
    
    url = 'https://api.telegram.org/bot'

    def __init__(self, token, master_id=None, log=False, logfile=None):
        self.token = Bot.validate_token(token)
        self.master_id = master_id  # only my telegram id
        self.url = Bot.url + self.token + '/'
        self.log = log
        self.log_file = logfile
        self.offset = 0
        self.handlers = {}
        self.helps = OrderedDict()
    
    @staticmethod
    def validate_token(token):
        if len(token) < 40 or ' ' in token:
            raise InvalidToken()
        return token
    
    def logger(self, log_message):
        with open(self.log_file, 'a') as logfile:
            logfile.write(str(log_message))
            logfile.write('\n\n')

    def add_handler(self, text, command, help_message=None):
        self.handlers[text] = command
        if help_message:
            self.helps[text] = help_message
        else:
            self.helps[text] = 'Описание отсутствует'

    def remove_handler(self, text):
        if self.handlers.get(text):
            del self.handlers[text]
            del self.helps[text]

    def print_help(self, chat_id, text):
        message = 'Список доступных боту команд:\n'
        for key in self.helps:
            message += '*{}* - {}\n'.format(key, self.helps[key])
        self.send_message(chat_id, message, parse_mode='Markdown')

    def get_updates(self, offset=0, timeout=100, limit=100, delay=1):
        url = self.url + 'getUpdates'
        params = { 'timeout' : timeout }
        if limit:
            params['limit'] = limit
        if offset:
            params['offset'] = offset
        response = requests.post(url, data=params)
        json_resp = response.json()
        if self.log:
            self.logger(result)
        if json_resp.get('ok') and json_resp.get('result'):
            result = json_resp.get('result')[0]
            if result.get('edited_message'):
                return int(result.get('update_id')) + 1
            chat_id = result.get('message').get('from').get('id')
            if self.master_id:
                if chat_id == self.master_id:
                    self.handle(chat_id, result.get('message').get('text'))
            else:
                self.handle(chat_id, result.get('message').get('text'))
            return int(result.get('update_id')) + 1
        return 0
    
    def start_polling(self, delay=1):
        try:
            while True:
                self.offset = self.get_updates(offset=self.offset)
                time.sleep(delay)
        except Exception as e:
            if self.log_file:
                with open(self.log_file, 'a') as errorlog:
                    errorlog.write('\tError: ' + str(e))
            time.sleep(20)
            self.offset += 1
            self.start_polling()

    def handle(self, chat_id, text):
        for key in self.handlers.keys():
            if text.startswith(key):
                result = self.handlers.get(key)(self, chat_id, text)
                break
        else:
            self.print_help(chat_id, 'text')

    def send_message(self, chat_id, text, keyboard=None, disable_notification=False, parse_mode=None):
        if text:
            params = {'chat_id': chat_id, 'text': text}
            if keyboard:
                params['reply_markup'] = keyboard
            if disable_notification:
                params['disable_notification'] = True
            if parse_mode:
                params['parse_mode'] = parse_mode
            response = requests.post(self.url + 'sendMessage', data=params)
            if self.log:
                self.logger(response.content)
            return response

    def send_photo(self, chat_id, photo, caption=None, keyboard=None):
        files = {'photo': open(photo, 'rb')}
        params = {'chat_id': chat_id}
        if caption:
            params['caption'] = caption
        if keyboard:
            params['reply_markup'] = keyboard
        response = requests.post(self.url + 'sendPhoto', files=files, data=params)
        if self.log:
            self.logger(response.content)
        return response

    def send_audio(self, chat_id, audio, caption=None, title=None):
        files = {'audio': open(audio, 'rb')}
        params = {'chat_id': chat_id}
        if caption:
            params['caption'] = caption
        if title:
            params['title'] = title
        response = requests.post(self.url + 'sendAudio', files=files, data=params)
        if self.log:
            self.logger(response.content)
        return response
        

def get_token(token_path):
    with open(token_path, 'r') as token_file:
        token = token_file.read().strip()
        return token

